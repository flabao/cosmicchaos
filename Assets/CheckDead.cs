﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class CheckDead : MonoBehaviour {
    public GameObject head;
    public GameObject loseMsg;
	// Use this for initialization
	void Start () {
        //head = GameObject.Find("PrefabRig/Camera (head)/Sphere");
        //loseMsg = GameObject.Find("YouLose");
    }
	
	// Update is called once per frame
	void Update () {
        var player = head.gameObject.GetComponent<PlayerHealth>();

        if (player.health <= 0) {
            print("You dead");
            
            
            StartCoroutine("waitThreeSeconds");
            
        }
	}

    IEnumerator waitThreeSeconds() {
        loseMsg.GetComponent<Text>().enabled = true;
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Lobby");
    }
}
