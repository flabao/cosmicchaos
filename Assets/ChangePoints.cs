﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePoints : MonoBehaviour {

    public GameObject head;
    private PlayerHealth player;

	// Use this for initialization
	void Start () {
        player = head.gameObject.GetComponent<PlayerHealth>();
        
        	
	}
	
	// Update is called once per frame
	void Update () {

        this.GetComponent<Text>().text = player.points.ToString();
	}
}
