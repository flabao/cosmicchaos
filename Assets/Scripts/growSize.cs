﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class growSize : MonoBehaviour {
    public Vector3 result;
    public Vector3 from = new Vector3(0.0000000000001f, 0.0000000000001f, 0.0000000000001f);
    public Vector3 to = new Vector3(4f, 4f, 4f);

    public Vector3 contEndpos;
    public Transform contEnd;
    public Vector3 contEndforward;
    public SteamVR_Controller.Device contDevice;
    // Use this for initialization
    void Start () {
        result = new Vector3(0.1f,0.1f,0.1f);

        var trackedObj = this.GetComponent<SteamVR_TrackedObject>();
        contDevice = SteamVR_Controller.Input((int)trackedObj.index);
    }
	
	// Update is called once per frame
	void Update () {

        contEndpos = this.transform.position;
        contEnd = this.GetComponent<Transform>();
        contEndforward = this.transform.forward;

        /*var newScale: float = Mathf.Lerp(1, 5, Time.deltaTime / 10);
        transform.localScale = Vector3(newScale, newScale, 1);*/
        Vector3 limit = new Vector3(1.2f, 10f, 10f);
        //transform.position = Vector3.MoveTowards(transform.position, contEndpos, 1f);
        if (transform.localScale.x < limit.x) {
            //Vector3 result = Vector3.Lerp(from, to, Time.deltaTime /20);
            //transform.localScale += result;
            //transform.localScale += from;
            
            transform.localScale = Vector3.Lerp(transform.localScale, to, 0.1f * Time.deltaTime);
            //GetComponent<Rigidbody>().isKinematic = true;

            
        }
        else {
            //transform.parent = null;
            GetComponent<Rigidbody>().isKinematic = false;
        }
}
}
