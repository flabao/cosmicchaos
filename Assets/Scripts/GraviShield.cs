﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraviShield : MonoBehaviour {

    public float graviForce = 1000f;
    private bool isActive = false;
    private Collider theCollider;


	// Use this for initialization
	void Start () {
        theCollider = GetComponentInChildren<Collider>();
        theCollider.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

    private void OnTriggerStay(Collider other) {
        if (isActive) {
            if (other.tag == "Planet") {
                other.GetComponent<Rigidbody>().AddForce(this.transform.forward * graviForce);
            } else if (other.tag == "GasGiant") {
                other.GetComponent<Rigidbody>().AddForce(this.transform.forward * graviForce/1.5f);
            }
        }
        
    }

    public void turnOn() {
        isActive = true;
        GetComponent<ParticleSystem>().Play();
        theCollider.enabled = true;
    }

    public void turnOff() {
        isActive = false;
        GetComponent<ParticleSystem>().Stop();
        theCollider.enabled = false;
    }

    public bool isOn() {
        return isActive;
    }

}
