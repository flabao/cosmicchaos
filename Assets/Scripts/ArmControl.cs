﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmControl : MonoBehaviour {

    public GameObject shootProj;
    public float laserRange = 100f;
    public float laserPower = 5f;
    public GameObject laserParticles;
    public GameObject lasParticles2;

    private SteamVR_TrackedObject trackable;
    private SteamVR_Controller.Device device;
    private LineRenderer line;
    private GraviShield gShield;
    private Light flashLight;
    private Grabber theGrabber;
    private GameObject tutText;

	// Use this for initialization
	void Start () {
        trackable = GetComponent<SteamVR_TrackedObject>();
        device = SteamVR_Controller.Input((int)trackable.index);
        line = GetComponent<LineRenderer>();
        gShield = transform.GetChild(2).GetComponent<GraviShield>();
        flashLight = GetComponentsInChildren<Light>()[0];
        flashLight.enabled = false;
        theGrabber = GetComponentsInChildren<Grabber>()[0];
        tutText = GameObject.Find("Text_Tut");
    }
	
	// Update is called once per frame
	void Update () {
        bool trigger = device.GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
        bool homeBut = device.GetPressDown(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu);
        bool grip = device.GetPress(Valve.VR.EVRButtonId.k_EButton_Grip);
        bool touchPadDown = device.GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
        bool touchPadTap = device.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
        
        Vector2 touchPadAxis = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
        
        
        if (touchPadTap && touchPadAxis.y > 0) {
            device.TriggerHapticPulse(2000);
            GameObject proj = (GameObject) Instantiate(shootProj, trackable.transform.position+(trackable.transform.forward/2), trackable.transform.rotation);
            proj.GetComponent<Rigidbody>().AddForce(trackable.transform.forward * 2000);
            TimedKill timeLimit = proj.AddComponent<TimedKill>();
            timeLimit.timeLeft = 10;

        }
        if (trigger) {
            Vector3 rayOri = trackable.transform.position;
            Vector3 rayForward = trackable.transform.forward;

            device.TriggerHapticPulse(100);

            line.SetPosition(0, rayOri);
            flashLight.enabled = true;

            RaycastHit collision;

            if (Physics.Raycast(rayOri, rayForward, out collision, laserRange)) {
                line.SetPosition(1, collision.point);
                if (collision.collider.tag == "GasGiant") {
                    GameObject lasPars = (GameObject)Instantiate(lasParticles2, collision.point, trackable.transform.rotation);
                    lasPars.transform.rotation = Quaternion.LookRotation(trackable.transform.position);
                } else {
                    GameObject lasPars = (GameObject)Instantiate(laserParticles, collision.point, trackable.transform.rotation);
                }
                
                CelestBody celest = collision.collider.GetComponent<CelestBody>();
                if (celest) {
                    celest.health -= laserPower;
                } else {
                    celest = collision.collider.GetComponentInParent<CelestBody>();
                    if (celest) {
                        celest.health -= laserPower;
                    }
                }
            } else {
                line.SetPosition(1, rayOri + trackable.transform.forward * laserRange);
            }
            line.enabled = true;
        } else {
            line.enabled = false;
            flashLight.enabled = false;
        }
        if (grip) {
            if (!gShield.isOn()) {
                gShield.turnOn();
                device.TriggerHapticPulse(300);
            }
        } else {
            if (gShield.isOn()) {
                gShield.turnOff();
            }
        }

        if (touchPadDown && touchPadAxis.y < 0) {
            theGrabber.turnOn();
        } else {
            if (theGrabber.isOn()) {
                theGrabber.turnOff(device.velocity*5, device.angularVelocity);
            }
            
        }

        if (homeBut) {
            if (tutText) {
                if (tutText.GetComponent<Text>().enabled==true) {
                    tutText.GetComponent<Text>().enabled = false;
                }
                else
                    tutText.GetComponent<Text>().enabled = true; ;
            }
            else {
                print("Can't find tut, buddy");
            }
        }
	}
}
