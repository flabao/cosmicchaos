﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderOfErase : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        CelestBody theCelestialBody = other.GetComponent<CelestBody>();
        if (theCelestialBody) {
            theCelestialBody.shrinkDestroy();
        }
    }
}
