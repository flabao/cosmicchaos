﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeGalaxy : MonoBehaviour {

    private int safeCount;

    public GameObject head;

	// Use this for initialization
	void Start () {
        
	}

    private void OnTriggerEnter(Collider other) {
        CelestBody otherBody = other.GetComponent<CelestBody>();
        if (otherBody && otherBody.tag == "Earth") {
            otherBody.shrinkDestroy();
            PlayerHealth thePoints = head.GetComponent<PlayerHealth>();
            if (thePoints) {
                thePoints.points++;
            }
            //print("NUMBER OF PLANETS SAVED: " + safeCount);
        }
    }
}
