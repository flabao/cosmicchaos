﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedKill : MonoBehaviour {

    public float timeLeft = 5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (timeLeft <= 0) {
            Destroy(this.gameObject);
        }
        timeLeft -= Time.deltaTime;
	}
}
