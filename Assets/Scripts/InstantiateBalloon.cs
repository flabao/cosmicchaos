﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateBalloon : MonoBehaviour {
    //for rigidbodies of balloons
    public Rigidbody blueBall;
    public Rigidbody redBall;
    public Rigidbody yellowBall;
    public Rigidbody greenBall;

    public Rigidbody currentBall;

    //for coordinates of controller
    public Vector3 contEndpos;
    public Transform contEnd;
    public Vector3 contEndforward;
    public SteamVR_Controller.Device contDevice;

    public int scalingFramesLeft = 0;

    public WaitForSeconds timer = new WaitForSeconds(2f);

    //For raycast shooting and ray Viewer
    public int gunDamage = 1;
    public float fireRate = .25f;
    public float weaponRange = 50f;
    public float hitForce = 100f;

    public Transform gunEnd;

    private Camera fpsCam;
    private WaitForSeconds shotDuration = new WaitForSeconds(.07f);
    private LineRenderer laserLine;
    private float nextFire;

    

    // Use this for initialization
    void Start () {
        var trackedObj = this.GetComponent<SteamVR_TrackedObject>();
        contDevice = SteamVR_Controller.Input((int)trackedObj.index);

        laserLine = GetComponent<LineRenderer>();
    }
	
	// Update is called once per frame
	void Update () {/*
        for (int i = 0; i < 1000; i++) {
            print("I'm Batman");
            transform.position += Vector3.up;
        }*/

            //contEnd = this.GetComponent<Transform>();
            contEndpos = this.transform.position;
            contEnd = this.GetComponent<Transform>();
            contEndforward = this.transform.forward;
            //print(contEndpos);
            //print(contEndrotation);

            //Picking balloon colour


            if (contDevice.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad)) {

                Vector2 touchButton = contDevice.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
                print("Pressing touchpad");


                if (touchButton.y > 0.6f) {
                    print("Moving Up");
                    currentBall = makeBallAppear(blueBall);

                    //currentBall.transform.position = contEndpos;
                    //changeBallScale();
                }
                if (touchButton.y < -0.6f) {
                    print("Moving Down");
                    currentBall = makeBallAppear(redBall);
                }
                if (touchButton.x > 0.6f) {
                    print("Moving Right");
                    currentBall = makeBallAppear(yellowBall);
                }
                if (touchButton.x < -0.6f) {
                    print("Moving left");
                    currentBall = makeBallAppear(greenBall);
                }

            }

            if (contDevice.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
                currentBall.transform.position = contEndpos;
            /*
            currentBall.isKinematic = true;
            currentBall.position = contEndpos;
            //ballGrowth();
            //releaseBall(currentBall);
            currentBall.isKinematic = false;
            //releaseBall(currentBall);
            */
            //confetti
            if (contDevice.GetPressDown(Valve.VR.EVRButtonId.k_EButton_Grip)) { }

            //shooting laser
            if (contDevice.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger)) {
                nextFire = Time.time + fireRate;

                StartCoroutine(ShotEffect());

                Vector3 rayOrigin = contEndpos;
                RaycastHit hit;

                laserLine.SetPosition(0, contEndpos);

                if (Physics.Raycast(rayOrigin, contEndforward, out hit, weaponRange)) {
                    laserLine.SetPosition(1, hit.point);
                    Damage health = hit.collider.GetComponent<Damage>();
                    if (health != null) {
                        health.Hurt(gunDamage);
                    }
                    if (hit.rigidbody != null) {
                        hit.rigidbody.AddForce(-hit.normal * hitForce);
                    }
                }
                else {
                    laserLine.SetPosition(1, rayOrigin + contEndforward * weaponRange);
                }
            }

            //vieweing ray
            Vector3 lineOrigin = contEndpos;
            Debug.DrawRay(lineOrigin, contEndforward * weaponRange, Color.green);
        
    }

    private IEnumerator ShotEffect() {
        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }

    private IEnumerator ballGrowth() {
        currentBall.position = contEndpos;
        yield return timer;
    }

    public Rigidbody makeBallAppear(Rigidbody ball)
    {
        Rigidbody balloonInstance;
        balloonInstance = Instantiate(ball, contEndpos, contEnd.rotation) as Rigidbody;
        
        //balloonInstance.isKinematic = true;
        releaseBall(balloonInstance);

        return balloonInstance;
    }
    public void releaseBall(Rigidbody balloonInstance) {
        balloonInstance.AddForce(contEndforward * 200);

        balloonInstance.AddForce(0, 200, 0);
    }
    
}
