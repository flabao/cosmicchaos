﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabber : MonoBehaviour {

    public float range = 1000f;
    public float pullingForce = 250000f;

    private LineRenderer line;
    private bool grabLaserOn;

    // Use this for initialization
    void Start() {
        line = GetComponent<LineRenderer>();
        line.enabled = false;
        grabLaserOn = false;
    }

    // Update is called once per frame
    void Update() {
        Vector3 ori = this.transform.position;
        Vector3 forw = this.transform.forward;
        FixedJoint aJoint = GetComponent<FixedJoint>();

        if (grabLaserOn && aJoint == null) {
            RaycastHit col;
            line.enabled = true;
            line.SetPosition(0, ori);

            if (Physics.Raycast(ori, forw, out col, range)) {
                line.SetPosition(1, col.point);
                if (col.collider.tag == "Planet" || col.collider.tag == "GasGiant"||col.collider.tag=="Earth") {
                    Rigidbody otherRig = col.collider.GetComponent<Rigidbody>();
                    otherRig.AddForce(this.transform.forward * -pullingForce);
                    CelestBody theBody;
                    theBody = col.collider.GetComponent<CelestBody>();
                    /*if (theBody.earthPlanet) {
                        col.collider.transform.localScale = Vector3.Lerp(col.collider.transform.localScale, new Vector3(0.1f,0.1f,0.1f), 0.01f);
                    }*/
                }
            }
            else {
                line.SetPosition(1, ori + forw * range);
            }
        } else {
            line.enabled = false;
        }

        if (aJoint && aJoint.connectedBody == null) {
            Destroy(GetComponent<FixedJoint>());
        }

    }

    private void OnTriggerStay(Collider collision) {
        if (grabLaserOn && !GetComponent<FixedJoint>() && collision.GetComponent<CelestBody>().earthPlanet) {
            FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
            var laRigid = collision.GetComponent<Rigidbody>();
            laRigid.transform.position = this.transform.position + this.transform.forward * collision.transform.localScale.x / 2;
            joint.connectedBody = laRigid;

        }


    }

    public void turnOn() {
        grabLaserOn = true;
    }

    public bool isOn() {
        return grabLaserOn;
    }

    public void turnOff(Vector3 givenVelo, Vector3 givenAngu) {
        FixedJoint theConnector = GetComponent<FixedJoint>();
        if (theConnector) {
            if (theConnector.connectedBody) {
                Rigidbody theRigid = theConnector.connectedBody;
                theConnector.connectedBody = null;
                theRigid.velocity = givenVelo;
                theRigid.angularVelocity = givenAngu;
            }
        }
        grabLaserOn = false;
    }
}
