﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrueBillBoard : MonoBehaviour {

    public Camera theObj;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(transform.position + theObj.transform.rotation * Vector3.forward,
            theObj.transform.rotation * Vector3.up);
    }
}
