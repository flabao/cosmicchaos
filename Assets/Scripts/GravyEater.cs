﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravyEater : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Planet" || other.tag == "GasGiant") {
            var celestial = other.GetComponent<CelestBody>();
            if (celestial) {
                if (celestial.shrinkCheck() < 0) {
                    celestial.shrinkDestroy();
                }
                
            }
        }
    }
}
