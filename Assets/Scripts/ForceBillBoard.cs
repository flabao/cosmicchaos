﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceBillBoard : MonoBehaviour {

    public Camera main_cam;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(transform.position + main_cam.transform.rotation * Vector3.forward, main_cam.transform.rotation*Vector3.up);
        //transform.LookAt(2 * transform.position - main_cam.transform.position);
    }
}
