﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    // Use this for initialization
    public float range = 10f;

    Rigidbody ownRb;

    //put own rigidbody into a variable
    void Start() {
        ownRb = GetComponent<Rigidbody>();
        //print(ownRb);
    }

    void FixedUpdate() {
        //make a collider array thats stores colliders within the specified range
        Collider[] cols = Physics.OverlapSphere(transform.position, range);
        List<Rigidbody> rbs = new List<Rigidbody>();

        //go thru every collider in cols and do it
        foreach (Collider c in cols) {
            Rigidbody rb = c.attachedRigidbody;
            //print(rb);
            if (rb != null && rb != ownRb && !rbs.Contains(rb)) {
                rbs.Add(rb);
                Vector3 offset = transform.position - c.transform.position;
                rb.AddForce(offset / offset.sqrMagnitude * ownRb.mass);
            }
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
