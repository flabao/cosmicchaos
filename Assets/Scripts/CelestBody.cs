﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelestBody : MonoBehaviour {

    public float health = 200f;
    public float power = 200f;
    public float adjustedScaler = 1f;
    public float shrapSpawnRange = 3f;
    public float burnWeakness = -1;
    public float gasPower = 0f;

    public GameObject shrapnelCelest;
    public GameObject shatterPiece;
    public GameObject explosionEffect;
    public GameObject burnEffect;
    public bool earthPlanet = false;

    private bool merger = false;

    private float density;
    private int shrinkDes;
    private int lastDamageType;
    private Rigidbody theRigid;

	// Use this for initialization
	void Start () {
        theRigid = GetComponent<Rigidbody>();
        density = theRigid.mass / transform.localScale.z;
        lastDamageType = 0;
        shrinkDes = -1;
	}

    // Update is called once per frame
    void Update() {
        if (health <= 0) {
            switch (lastDamageType) {
                case 0:
                    shatterDestroy();
                    break;
                case 1:
                    burnDestroy();
                    break;
                case 2:
                    shatterDestroy();
                    break;
                default:
                    shrinkDestroy();
                    break;
            }
            
        }
        if (shrinkDes > 0) {
            shrinkDes -= 1;
            transform.localScale /= 1.05f;
            if (shrinkDes == 0) {
                Destroy(this.gameObject);
            }
        }

	}

    private void OnCollisionEnter(Collision collision) {
        var player = collision.gameObject.GetComponent<PlayerHealth>();
        if (collision.gameObject.tag == "Player") {
            
            if (this.tag == "Planet") {
                player.health = player.health - 1 ;

                shatterDestroy();

            }
            if (tag == "GasGiant") {
                player.health = player.health - 2;
                shatterDestroy();
            }
            

            
        }

        else if(tag == "Earth") {
            /*if (collision.gameObject.tag == "Galaxy") {
                player.points= player.points +1;
            }*/
            if (collision.gameObject.tag == "Player") {
                this.health++;
            }
            else if (collision.gameObject.tag == "Planet" || collision.gameObject.tag == "GasGiant" || collision.gameObject.tag == "Earth") {
                var otherCelestBody = collision.gameObject.GetComponent<CelestBody>();
                this.health -= otherCelestBody.power * (otherCelestBody.GetComponent<Rigidbody>().velocity - theRigid.velocity).magnitude / 10;
                lastDamageType = 0;
            }
            else if (collision.gameObject.tag == "Blackhole") {
                Destroy(this.gameObject);
            }
        }

        else if (tag == "Planet") {
            if (collision.gameObject.tag == "Planet" || collision.gameObject.tag == "GasGiant" || collision.gameObject.tag=="Earth") {
                var otherCelestBody = collision.gameObject.GetComponent<CelestBody>();
                this.health -= otherCelestBody.power * (otherCelestBody.GetComponent<Rigidbody>().velocity - theRigid.velocity).magnitude / 10;
                lastDamageType = 0;
            } else if (collision.gameObject.tag == "Blackhole") {
                Destroy(this.gameObject);
            }
        } else if (tag == "GasGiant") {
            if (collision.gameObject.tag == "Planet" || collision.gameObject.tag=="Earth") {
                var otherCelestBody = collision.gameObject.GetComponent<CelestBody>();
                this.health -= otherCelestBody.power * (otherCelestBody.GetComponent<Rigidbody>().velocity - theRigid.velocity).magnitude / 10;
                lastDamageType = 0;
            } else if (collision.gameObject.tag == "GasGiant") {
                var otherCelestBody = collision.gameObject.GetComponent<CelestBody>();
                if (shrinkDes < 0) {
                    otherCelestBody.shrinkDestroy();
                    this.health += otherCelestBody.health;
                }
            } else if (collision.gameObject.tag == "Blackhole") {
                Destroy(this.gameObject);
            }
            
        }
        
    }

    private void shatterDestroy() {
        if (explosionEffect) {
            GameObject explosionParticles = (GameObject)Instantiate(explosionEffect, this.transform.position, this.transform.rotation);
            explosionParticles.transform.localScale = this.transform.localScale * 3 * adjustedScaler;
        }
        if (shatterPiece) {
            GameObject shatterParticles = (GameObject)Instantiate(shatterPiece, this.transform.position, this.transform.rotation);
        }
        if (shrapnelCelest) {
            for (int i = 0; i < 2; i++) {
                GameObject shrapnel = (GameObject)Instantiate(shrapnelCelest, this.transform.position + new Vector3(Random.Range(-shrapSpawnRange,shrapSpawnRange), Random.Range(-shrapSpawnRange, shrapSpawnRange), Random.Range(-shrapSpawnRange, shrapSpawnRange)), Random.rotation);
                shrapnel.GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity;
                shrapnel.GetComponent<Rigidbody>().angularVelocity = this.GetComponent<Rigidbody>().angularVelocity;
            }
            
        }
        Destroy(this.gameObject);
    }

    public void shrinkDestroy() {
        if (shrinkDes < 0) {
            shrinkDes = 100;
        }
    }

    public int shrinkCheck() {
        return shrinkDes;
    }

    private void OnTriggerStay(Collider c) {
        if (c.gameObject.tag == "Star") {
            if (burnWeakness == -1) {
                burnDestroy();
            } else {
                health -= burnWeakness/Mathf.Abs(Vector3.Distance(transform.position, c.transform.position));
                lastDamageType = 1;
            }
            
        } else if (c.gameObject.tag == "GasGiant") {
            if (tag == "Planet" || tag=="Earth") {
                health -= c.GetComponent<CelestBody>().gasPower;
                lastDamageType = 2;
            }
        } else if (c.gameObject.tag == "Player") {
            var player = c.gameObject.GetComponent<PlayerHealth>();
            if (tag == "GasGiant") {
                player.health = player.health - 2;
                shatterDestroy();
            }
        }
    }
    private void burnDestroy() {
        if (burnEffect) {
            var particleObject = Instantiate(burnEffect, transform.position, transform.rotation);
            particleObject.transform.localScale = this.transform.localScale * adjustedScaler;
        }
        Destroy(gameObject);
    }
}
