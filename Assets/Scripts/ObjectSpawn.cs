﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawn : MonoBehaviour {

    public Rigidbody planetA;
    public Rigidbody planetB;
    public Rigidbody planetC;

    public Rigidbody gasGiantA;

    public Rigidbody earthPlanetA;

    public Rigidbody starA;
    public Rigidbody starB;
    public Rigidbody starC;

    public GameObject targeting;

    public int spawnType = 0;

    private int spawnTime;
    private float spawnSize;
    private bool spawning;

	// Use this for initialization
	void Start () {
        spawning = true;
        spawnTime = 50;
        spawnSize = 5;
    }
	
	// Update is called once per frame
	void Update () {
        
        if (spawning) {
            int choice = Random.Range(0, 5);
            float spawnRange = Random.Range(-50f, 50f);
            float spawnRangeB = Random.Range(-50f, 50f);
            float spawnZBack = spawnSize + 10f;
            Rigidbody spawnedObject;
            if (spawnTime <= 0) {
                Vector3 spawnPoint = transform.position - transform.forward * spawnZBack + new Vector3(spawnRange, spawnRangeB, 0);
                if (spawnType == 1) {
                    choice = 0;
                }
                else if (spawnType == 2) {
                    choice = 3;
                }
                Vector3 offTarget = new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0);
                if (choice == 0) {
                    spawnedObject = Instantiate(planetA, spawnPoint, Quaternion.LookRotation((targeting.transform.position + offTarget - spawnPoint).normalized));
                    spawnedObject.GetComponent<Rigidbody>().mass *= 10;
                    spawnedObject.GetComponent<CelestBody>().health = 10;
                    spawnedObject.transform.localScale *= 1.5f;
                    
                    if (spawnType == 1) {
                        spawnedObject.AddForce(spawnedObject.transform.forward * Random.Range(30000,70000));
                        spawnTime = Random.Range(10, 80);
                    } else {
                        spawnedObject.AddForce(spawnedObject.transform.forward * 50000);
                    }
                    
                }
                else if (choice == 1) {
                    spawnedObject = Instantiate(planetB, spawnPoint, Quaternion.LookRotation((targeting.transform.position + offTarget - spawnPoint).normalized));
                    spawnedObject.AddForce(spawnedObject.transform.forward * Random.Range(100000,300000));
                    spawnedObject.transform.rotation = Random.rotation;
                }
                else if (choice == 2) {
                    spawnedObject = Instantiate(planetC, spawnPoint, Quaternion.LookRotation((targeting.transform.position + offTarget - spawnPoint).normalized));
                    spawnedObject.AddForce(spawnedObject.transform.forward * 500000);
                    spawnedObject.transform.rotation = Random.rotation;
                }
                else if (choice == 3) {
                    spawnedObject = Instantiate(gasGiantA, spawnPoint, Quaternion.LookRotation((targeting.transform.position + offTarget - spawnPoint).normalized));
                    spawnedObject.AddForce(spawnedObject.transform.forward * Random.Range(150000,700000));
                    spawnedObject.transform.rotation = Random.rotation;
                }
                else if (choice == 4) {
                    spawnedObject = Instantiate(earthPlanetA, spawnPoint, Quaternion.LookRotation((targeting.transform.position + offTarget - spawnPoint).normalized));
                    spawnedObject.AddForce(spawnedObject.transform.forward * Random.Range(100000, 250000));
                    spawnedObject.transform.rotation = Random.rotation;
                }
                if (spawnTime <= 0) {
                    spawnTime = Random.Range(60, 300);
                }
                
                
            }
            spawnTime--;
        }
	}
}
