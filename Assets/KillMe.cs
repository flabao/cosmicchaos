﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMe : MonoBehaviour {

    public GameObject burnEffect;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Destroy(gameObject, 5.0f);
        
	}
    private void OnCollisionEnter(Collision c) {
        if(c.gameObject.name=="Black Hole") {
            Destroy(gameObject);
        }
        if (c.gameObject.tag == "Star") {
            if (burnEffect) {
                var particleObject = Instantiate(burnEffect, transform.position, transform.rotation);
                particleObject.transform.localScale = this.transform.localScale;
            }
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider c) {
        if (c.gameObject.tag == "Star") {
            if (burnEffect) {
                var particleObject = Instantiate(burnEffect, transform.position, transform.rotation);
                particleObject.transform.localScale = this.transform.localScale;
            }
            Destroy(gameObject);
        }
    }
}
