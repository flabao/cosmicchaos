﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    public float MAXHEALTH = 10f;
    public float health = 10f;
    public float points = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void loseHealth(int x) {
        health = health - x;

    }
    void addPoints(int x) {
        points = points - x;
    }
}
