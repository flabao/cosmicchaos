﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Wacki {

    public class DemoMenu : MonoBehaviour {

        public Transform dirLight;
        public GameObject cubeSpawn;
        public GameObject planet;

        public void LoadEndless() {
            SceneManager.LoadScene("SpaceScene");
        }

        public void RotateLight(float amount)
        {
            dirLight.rotation = Quaternion.AngleAxis(amount, Vector3.right);
        }

        public void SpawnCube()
        {
            var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            go.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            go.transform.position = cubeSpawn.transform.position;
            go.transform.rotation = cubeSpawn.transform.rotation;
            go.AddComponent<Rigidbody>();
        }

        public void SpawnPlanet() {
            //print(cubeSpawn.position.x);
            //Instantiate(planet, cubeSpawn.transform);
            Vector3 position = new Vector3(Random.Range(-40, 40), Random.Range(-10, 10), Random.Range(50, 50));
            Instantiate(planet, position, Quaternion.identity);
            
        }

        public void loadSaveEarth() {
            SceneManager.LoadScene("SaveEarth");
        }
    }

}