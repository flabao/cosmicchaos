﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUI : MonoBehaviour {

    public GameObject head;
    private PlayerHealth player;
    private float previous;

	// Use this for initialization
	void Start () {
        player = head.gameObject.GetComponent<PlayerHealth>();
        previous = player.health;
        
	}
	
	// Update is called once per frame
	void Update () {
        
        if (player.health != previous) {
            
            float ratio = player.health / player.MAXHEALTH;

            //this.transform.localScale = this.transform.localScale * ratio;
            this.transform.localScale = new Vector3(0.01f, ratio / 10f, 0.01f);
            print(ratio);
            previous = player.health;
            
        }
        
    }
}
